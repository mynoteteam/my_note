package maestro.mynote;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import maestro.mynote.ui.painter.DrawView;
import maestro.mynote.ui.painter.DrawView.OnDrawViewEventListener;

/**
 * Created by Artyom on 31.10.2015.
 */
public class DrawFragment extends Fragment implements OnDrawViewEventListener {

    public static final String TAG = DrawFragment.class.getSimpleName();

    private DrawView mDrawView;
    private LinearLayout mActionParent;

    private DecelerateInterpolator interpolator = new DecelerateInterpolator(1.3f);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.draw_fragment_view, null);
        mDrawView = (DrawView) v.findViewById(R.id.draw_view);
        mActionParent = (LinearLayout) v.findViewById(R.id.action_parent);

        mDrawView.addOnDrawViewEventListener(this);

        return v;
    }

    private void setActionVisibility(boolean visibile) {
        mActionParent.animate().alpha(visibile ? 1f : 0f).setInterpolator(interpolator).start();
    }

    @Override
    public void onDrawStart() {
        setActionVisibility(false);
    }

    @Override
    public void onDrawEnd() {
        setActionVisibility(true);
    }
}
