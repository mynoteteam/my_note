package maestro.mynote.widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import maestro.mynote.Main;
import maestro.mynote.R;

/**
 * Created by Artyom on 5/18/2015.
 */
public class AddWidget extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; i++) {
            int appWidgetId = appWidgetIds[i];
            Intent intent = new Intent(context, Main.class);
            intent.putExtra(Main.PARAM_OPEN_EDIT, true);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.add_widget_view);
            views.setOnClickPendingIntent(R.id.image, pendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }


}
