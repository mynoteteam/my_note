package maestro.mynote.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

/**
 * Created by Artyom on 10.11.2015.
 */
public class ConfirmDialogFragment extends DialogFragment {

    public static final String TAG = ConfirmDialogFragment.class.getSimpleName();

    private static final String PARAM_KEY = "key";

    public static ConfirmDialogFragment makeInstance(String key) {
        ConfirmDialogFragment fragment = new ConfirmDialogFragment();
        Bundle args = new Bundle(1);
        args.putString(PARAM_KEY, key);
        return fragment;
    }

    private DialogListener mListener;
    private String mKey;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), getTheme());
        DialogRetriever retriever = mListener.getRetriever(mKey);
        builder.setTitle(retriever.getTitle(getContext()));
        builder.setMessage(retriever.getContent(getContext()));
        builder.setPositiveButton(retriever.getPositiveText(getContext()), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onDialogConfirm(mKey);
            }
        });
        builder.setNegativeButton(retriever.getNegativeText(getContext()), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mListener.onDialogDismiss(mKey);
            }
        });
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (getArguments() != null) {
            mKey = getArguments().getString(PARAM_KEY);
        }
        if (getParentFragment() instanceof DialogListener) {
            mListener = (DialogListener) getParentFragment();
        } else if (getActivity() instanceof DialogListener) {
            mListener = (DialogListener) getActivity();
        } else {
            throw new IllegalStateException("No implementation of DialogListener in parent fragment or activity");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public static class DialogRetriever {

        public CharSequence getTitle(Context context) {
            return null;
        }

        public CharSequence getContent(Context context) {
            return null;
        }

        public CharSequence getPositiveText(Context context) {
            return context.getString(android.R.string.ok);
        }

        public CharSequence getNegativeText(Context context) {
            return context.getString(android.R.string.cancel);
        }

    }

    public interface DialogListener {
        void onDialogConfirm(String key);

        void onDialogDismiss(String key);

        DialogRetriever getRetriever(String key);
    }

}
