package maestro.mynote.ui;

import android.app.Activity;
import android.support.v4.app.DialogFragment;
import maestro.mynote.Main;
import maestro.mynote.utils.OnBackPressListener;

/**
 * Created by Artyom on 5/16/2015.
 */
public class BaseFragment extends DialogFragment implements OnBackPressListener {

    public static final String TAG = BaseFragment.class.getSimpleName();

    Main main;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Main) {
            main = (Main) activity;
            main.attachOnBackPressListener(this);
        }
    }

    @Override
    public void onDetach() {
        if (main != null) {
            main.detachOnBackPressListener(this);
            main = null;
        }
        super.onDetach();
    }

    @Override
    public boolean onBackPress() {
        return false;
    }
}
