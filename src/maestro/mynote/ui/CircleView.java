package maestro.mynote.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import maestro.mynote.utils.ColorCircle;

/**
 * Created by Artyom on 24.10.2015.
 */
public class CircleView extends View {

    public static final String TAG = CircleView.class.getSimpleName();

    private ColorCircle mCircleDrawable;

    public CircleView(Context context) {
        super(context);
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setColor(int color) {
        if (mCircleDrawable == null) {
            mCircleDrawable = new ColorCircle(color);
        } else {
            mCircleDrawable.setColor(color);
        }
    }

    public ColorCircle getCircleDrawable() {
        if (mCircleDrawable == null) {
            mCircleDrawable = new ColorCircle(Color.TRANSPARENT);
        }
        return mCircleDrawable;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mCircleDrawable != null) {
            int width = getWidth();
            int height = getHeight();
            int size = Math.min(width, height);
            int halfWidth = width / 2;
            int halfHeight = height / 2;
            int halfSize = size / 2;
            mCircleDrawable.setBounds(halfWidth - halfSize, halfHeight - halfSize,
                    halfWidth + halfSize, halfHeight + halfSize);
            mCircleDrawable.draw(canvas);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthSpec = MeasureSpec.getMode(widthMeasureSpec);
        int heightSpec = MeasureSpec.getMode(heightMeasureSpec);
        int maxWidth = MeasureSpec.getSize(widthMeasureSpec);
        int maxHeight = MeasureSpec.getSize(heightMeasureSpec);

        Log.e(TAG, "onMeasure: " + getMeasuredWidth() + "/" + getMeasuredHeight() + ", spec size: " + maxWidth + "/" + maxHeight);
    }

}
