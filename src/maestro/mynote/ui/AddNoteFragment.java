package maestro.mynote.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.*;
import android.view.animation.DecelerateInterpolator;
import android.widget.*;
import maestro.mynote.R;
import maestro.mynote.data.model.Note;
import maestro.mynote.ui.ConfirmDialogFragment.DialogListener;
import maestro.mynote.utils.Typefacer;
import maestro.mynote.utils.Utils;
import maestro.svg.SVGInstance;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Artyom on 5/17/2015.
 */
public class AddNoteFragment extends BaseFragment implements View.OnClickListener, DialogListener {

    public static final String TAG = AddNoteFragment.class.getSimpleName();

    private static final String PARAM_NOTE = "param_note";
    private static final String PARAM_COPY_NOTE = "param_copy_note";

    private static final int NOT_SET = -1;

    private static final int ACTION_ALARM = 0;
    private static final int ACTION_BIRTHDAY = 1;
    private static final int ACTION_COLOR = 2;
    private static final int ACTION_TYPE = 3;
    private static final int ACTION_SHARE = 4;

    private static final long ANIMATION_DELAY = 75;

    private ImageView mBackIcon;
    private ImageView mCloseIcon;
    private EditText mTitleEditText;
    private EditText mContentEditText;
    private LinearLayout mActionItemParent;
    private LinearLayout mActionPopupParent;
    private LinearLayout mActionPopupFrame;
    private Note mNote;
    private Note mCopyNote;

    private int mCurrentAction = NOT_SET;

    private static final DecelerateInterpolator ANIMATION_INTERPOLATOR_IN
            = new DecelerateInterpolator(1.3f);

    private static final DecelerateInterpolator ANIMATION_INTERPOLATOR_OUT
            = new DecelerateInterpolator(0.7f);

    public static AddNoteFragment makeInstance(Note note) {
        AddNoteFragment fragment = new AddNoteFragment();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_NOTE, note);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.MyNote_Theme_Overlay);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.AddViewAnimation);
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        //TODO: finally save or delete note
        super.onDismiss(dialog);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.add_note_view, null);
        mBackIcon = (ImageView) v.findViewById(R.id.back_icon);
        mCloseIcon = (ImageView) v.findViewById(R.id.close_icon);
        mTitleEditText = (EditText) v.findViewById(R.id.title_edit_text);
        mContentEditText = (EditText) v.findViewById(R.id.note_content_edit_text);
        mActionPopupFrame = (LinearLayout) v.findViewById(R.id.action_popup_frame);
        mActionItemParent = (LinearLayout) v.findViewById(R.id.edit_bottom_bar_icon_parent);
        mActionPopupParent = (LinearLayout) v.findViewById(R.id.action_popup_parent);
        mTitleEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    mContentEditText.requestFocus();
                    return true;
                }
                return false;
            }
        });
        mContentEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL && mContentEditText.getText().length() == 0) {
                    mTitleEditText.requestFocus();
                }
                return false;
            }
        });

        mTitleEditText.setTypeface(Typefacer.rCondensedItalic);
        mContentEditText.setTypeface(Typefacer.rCondensedItalic);

        SVGInstance.applySVG(mBackIcon, R.raw.back, Color.parseColor("#6d6d6d"));
        SVGInstance.applySVG(mCloseIcon, R.raw.close, Color.parseColor("#6d6d6d"));

        mActionItemParent.setOnClickListener(this);
        mBackIcon.setOnClickListener(this);
        mCloseIcon.setOnClickListener(this);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey(PARAM_NOTE)) {
            mNote = savedInstanceState.getParcelable(PARAM_NOTE);
            if (savedInstanceState.containsKey(PARAM_COPY_NOTE)) {
                mCopyNote = savedInstanceState.getParcelable(PARAM_COPY_NOTE);
            }
        } else if (getArguments() != null && getArguments().containsKey(PARAM_NOTE)) {
            mNote = getArguments().getParcelable(PARAM_NOTE);
            mCopyNote = mNote.clone();
        } else {
            mNote = new Note();
        }
        prepareActionItems();

        mTitleEditText.setText(mNote.getTitle());
        if (!TextUtils.isEmpty(mNote.getContent())) {
            mContentEditText.setText(mNote.getContent());
            mContentEditText.requestFocus();
            mContentEditText.setSelection(mContentEditText.getText().length());
        }

        mTitleEditText.addTextChangedListener(mTitleTextWatcher);
        mContentEditText.addTextChangedListener(mContentTextWatcher);
        applyColorFilter();

        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    return onBackPress();
                }
                return false;
            }
        });
    }

    @Override
    public boolean onBackPress() {
        if (isDeletable() || isChanged()) {
            new ConfirmDialogFragment().show(getChildFragmentManager(), ConfirmDialogFragment.TAG);
            return true;
        }
        return false;
    }

    private boolean isDeletable() {
        return mNote.isEmpty() && isCopyEmpty();
    }

    private boolean isChanged() {
        return (mCopyNote == null && !mNote.isEmpty()) || (mCopyNote != null && !mCopyNote.equals(mNote));
    }

    private boolean isCopyEmpty() {
        return mCopyNote != null && mCopyNote.isEmpty();
    }

    private void applyColorFilter() {
        final int color = Color.parseColor(mNote.getColor());
        final int textColor = Utils.isBrightColor(color) ? Color.BLACK : Color.WHITE;
        getView().findViewById(R.id.edit_top_bar_parent).setBackgroundColor(color);
        mTitleEditText.setTextColor(textColor);
        SVGInstance.applySVG(mBackIcon, R.raw.back, textColor);
        SVGInstance.applySVG(mCloseIcon, R.raw.close, textColor);
    }

    private void prepareActionItems() {
        ArrayList<ActionItem> items = collectActionItems();
        for (ActionItem actionItem : items) {
            ImageView imageView = new ImageView(getActivity());
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setTag(actionItem);
            imageView.setOnClickListener(mActionItemClickListener);
            imageView.setId(actionItem.Id);
            SVGInstance.applySVG(imageView, actionItem.IconResource, Color.parseColor("#6d6d6d"));
            mActionItemParent.addView(imageView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT, 1));
        }
    }

    private ArrayList<ActionItem> collectActionItems() {
        ArrayList<ActionItem> items = new ArrayList<>();
        items.add(new ActionItem(ACTION_ALARM, R.raw.alarm));
        items.add(new ActionItem(ACTION_BIRTHDAY, R.raw.cake));
        items.add(new ActionItem(ACTION_COLOR, R.raw.palette));
        items.add(new ActionItem(ACTION_TYPE, R.raw.format_list));
        items.add(new ActionItem(ACTION_SHARE, R.raw.share));
        return items;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_icon:
                dismiss();
                break;
            case R.id.close_icon:

                break;
            case R.id.edit_bottom_bar_icon_parent:
                if (mCurrentAction != NOT_SET) {
                    animatePopupFrame(false);
                }
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(PARAM_NOTE, mNote);
        if (mCopyNote != null) {
            outState.putParcelable(PARAM_COPY_NOTE, mCopyNote);
        }
    }

    private void animatePopupFrame(boolean visible) {
        if (visible) {
            if (mActionPopupFrame.getTranslationY() == 0) {
                mActionPopupFrame.setTranslationY(mActionPopupFrame.getHeight());
            }
            if (mActionPopupFrame.getAlpha() != 0) {
                mActionPopupFrame.setAlpha(0);
            }
            if (mActionPopupParent.getTranslationY() == 0) {
                mActionPopupParent.setTranslationY(mActionItemParent.getHeight());
            }
            if (mActionPopupParent.getAlpha() != 0) {
                mActionPopupParent.setAlpha(0);
            }
            mActionPopupFrame.animate().translationY(0).alpha(1f).setInterpolator(ANIMATION_INTERPOLATOR_IN).start();
            mActionPopupParent.animate().translationY(0).alpha(1f).setStartDelay(ANIMATION_DELAY)
                    .setInterpolator(ANIMATION_INTERPOLATOR_IN).start();
        } else {
            mCurrentAction = NOT_SET;
            mActionPopupFrame.animate().translationY(mActionPopupFrame.getHeight()).alpha(0f)
                    .setStartDelay(ANIMATION_DELAY).setInterpolator(ANIMATION_INTERPOLATOR_OUT).start();
            mActionPopupParent.animate().translationY(mActionPopupParent.getHeight()).alpha(0)
                    .setInterpolator(ANIMATION_INTERPOLATOR_OUT).start();
            animateIcons(NOT_SET);
        }
    }

    public void openNotifyAction() {
        animateIcons(ACTION_ALARM);
        mActionPopupParent.removeAllViews();
        mActionPopupParent.setOrientation(LinearLayout.VERTICAL);
        mActionPopupParent.addView(getActionItemRowView(R.string.tomorow));
        mActionPopupParent.addView(getActionItemRowView(R.string.after_three_days));
        mActionPopupParent.addView(getActionItemRowView(R.string.next_week));
        mActionPopupParent.addView(getActionItemRowView(R.string.set_custom_date));
        mActionPopupParent.setVisibility(View.VISIBLE);
        animatePopupFrame(true);
    }

    public void openColorAction() {
        animateIcons(ACTION_COLOR);
        mActionPopupParent.removeAllViews();
        mActionPopupParent.setOrientation(LinearLayout.HORIZONTAL);
        String[] colors = getResources().getStringArray(R.array.noteColors);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                getResources().getDimensionPixelSize(R.dimen.def_circle_size), 1);
        for (String color : colors) {
            mActionPopupParent.addView(getActionItemColorCircleView(color), params);
        }
        animatePopupFrame(true);
    }

    public void openBirthDayAction() {
        openDateSelectAction();
        animatePopupFrame(true);
    }

    public void openDateSelectAction() {
        final View selectParent = View.inflate(getContext(), R.layout.action_date_select, null);
        final Spinner daySpinner = (Spinner) selectParent.findViewById(R.id.day_spinner);
        final Spinner monthSpinner = (Spinner) selectParent.findViewById(R.id.month_spinner);
        final Spinner yearSpinner = (Spinner) selectParent.findViewById(R.id.year_spinner);

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        final IntFillAdapter dayAdapter = new IntFillAdapter(getContext(), calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        final IntFillAdapter monthAdapter = new IntFillAdapter(getContext(), 12);
        final IntFillAdapter yearAdapter = new IntFillAdapter(getContext(), new IntFillAdapter.DataRetriever() {
            @Override
            public String getTitle(int position) {
                return String.valueOf(1900 + position);
            }
        }, calendar.get(Calendar.YEAR) - 1900);

        daySpinner.setAdapter(dayAdapter);
        monthSpinner.setAdapter(monthAdapter);
        yearSpinner.setAdapter(yearAdapter);

        daySpinner.setSelection(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        monthSpinner.setSelection(calendar.get(Calendar.MONTH));
        yearSpinner.setSelection(calendar.get(Calendar.YEAR));

        daySpinner.setOnItemSelectedListener(mDateSelectListener);
        monthSpinner.setOnItemSelectedListener(mDateSelectListener);
        yearSpinner.setOnItemSelectedListener(mDateSelectListener);

        mActionPopupParent.removeAllViews();
        mActionPopupParent.addView(selectParent, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, getResources().getDisplayMetrics())));

    }

    private int pDay = NOT_SET;
    private int pMonth = NOT_SET;
    private int pYear = NOT_SET;

    private void animateIcons(int exceptId) {
        final int childCount = mActionItemParent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View v = mActionItemParent.getChildAt(i);
            if (exceptId == NOT_SET) {
//                v.animate().translationY(0).translationX(0).start();
                v.animate().alpha(1f).start();
            } else if (v.getId() == exceptId) {
//                float originX = v.getX();
//                float placeX = mActionItemParent.getWidth() / 2 - v.getWidth() / 2;
//                v.animate().translationX(placeX - originX).start();
            } else {
//                v.animate().translationY(v.getHeight()).translationX(0).start();
                v.animate().alpha(.15f).start();
            }
        }
    }

    private View getActionItemRowView(int title) {
        View v = View.inflate(getActivity(), R.layout.action_item_row_view, null);
        TextView titleView = (TextView) v.findViewById(R.id.title);
        titleView.setTypeface(Typefacer.rCondensedItalic);
        titleView.setText(title);
        return v;
    }

    private View getActionItemColorCircleView(final String color) {
        CircleView v = (CircleView) View.inflate(getActivity(), R.layout.action_item_color_circle_view, null);
        v.setColor(Color.parseColor(color));
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNote.setColor(color);
                if (!mNote.isEmpty()) {
                    mNote.save();
                }
                applyColorFilter();
            }
        });
        return v;
    }

    @Override
    public void onDialogConfirm(String key) {
        mNote.save();
        dismiss();
    }

    @Override
    public void onDialogDismiss(String key) {
        dismiss();
    }

    @Override
    public ConfirmDialogFragment.DialogRetriever getRetriever(String key) {
        return new ConfirmDialogFragment.DialogRetriever() {
            @Override
            public CharSequence getTitle(Context context) {
                return context.getString(R.string.note_save_prompt);
            }

            @Override
            public CharSequence getPositiveText(Context context) {
                return context.getString(R.string.save);
            }
        };
    }

    private View.OnClickListener mActionItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (mCurrentAction != -1) {
                animatePopupFrame(false);
                return;
            }

            ActionItem actionItem = (ActionItem) v.getTag();
            mCurrentAction = actionItem.Id;
            switch (actionItem.Id) {
                case ACTION_ALARM:
                    openNotifyAction();
                    break;
                case ACTION_COLOR:
                    openColorAction();
                    break;
                case ACTION_BIRTHDAY:
                    openBirthDayAction();
                    break;
            }
        }
    };

    private AdapterView.OnItemSelectedListener mDateSelectListener = new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            switch (parent.getId()) {
                case R.id.day_spinner:
                    if (pDay != position) {
                        pDay = position;
                    }
                    break;
                case R.id.month_spinner:
                    if (pMonth != position) {
                        pMonth = position;
                    }
                    break;
                case R.id.year_spinner:
                    if (pYear != position) {
                        pYear = position;
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private TextWatcher mContentTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            mNote.setContent(s.toString());
//            mNote.save();
        }
    };

    private TextWatcher mTitleTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            mNote.setTitle(s.toString());

        }
    };

    public static final class ActionItem {

        private int Id;
        private int IconResource;

        public ActionItem(int id, int iconResource) {
            Id = id;
            IconResource = iconResource;
        }

    }

    public static final class IntFillAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private DataRetriever mRetriever;
        private int mCount;

        public interface DataRetriever {
            String getTitle(int position);
        }

        public IntFillAdapter(Context context, int count) {
            mCount = count;
            mInflater = LayoutInflater.from(context);
        }

        public IntFillAdapter(Context context, DataRetriever retriever, int count) {
            this(context, count);
            mRetriever = retriever;
        }

        public void update(int count) {
            mCount = count;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mCount;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                holder = new Holder();
                convertView = mInflater.inflate(R.layout.single_list_item, null);
                holder.Text = (TextView) convertView.findViewById(R.id.text);
                convertView.setTag(holder);
            } else {
                holder = (Holder) convertView.getTag();
            }
            holder.Text.setText(mRetriever != null ? mRetriever.getTitle(position) : String.valueOf(position + 1));
            return convertView;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getView(position, convertView, parent);
        }

        class Holder {
            TextView Text;
        }

    }

}
