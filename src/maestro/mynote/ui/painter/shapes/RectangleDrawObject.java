package maestro.mynote.ui.painter.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

/**
 * Created by Artyom on 03.11.2015.
 */
public class RectangleDrawObject extends ShapeDrawObject {

    public static final String TAG = RectangleDrawObject.class.getSimpleName();

    private RectF rect = new RectF();

    private float centerX = NOT_SET;
    private float centerY = NOT_SET;

    private DRAG_TYPE mDragType = DRAG_TYPE.FIXED;

    public enum DRAG_TYPE {
        SIDE, SCALE, FIXED
    }

    public RectangleDrawObject(DrawCallback callback) {
        super(callback);

        getPaint().setStyle(Paint.Style.STROKE);

    }

    @Override
    public boolean move(float x, float y) {
        if (centerX == NOT_SET || centerY == NOT_SET) {
            centerX = x;
            centerY = y;
        } else {
            switch (mDragType) {
                case FIXED:
                    float absSize = (Math.abs(centerX - x) + Math.abs(centerY - y));
                    rect.set(centerX - absSize / 2, centerY - absSize / 2, centerX + absSize / 2, centerY + absSize / 2);
                    break;
                case SCALE:
                    float width = Math.abs(centerX - x) * 2f;
                    float height = Math.abs(centerY - y) * 2f;
                    rect.set(centerX - width / 2, centerY - height / 2, centerX + width / 2, centerY + height / 2);
                    break;
                case SIDE:
                    width = Math.abs(centerX - x);
                    height = Math.abs(centerY - y);
                    if (x > centerX) {
                        rect.left = centerX;
                        rect.right = centerX + width;
                    } else {
                        rect.left = centerX - width;
                        rect.right = centerX;
                    }
                    if (y > centerY) {
                        rect.top = centerY;
                        rect.bottom = centerY + height;
                    } else {
                        rect.top = centerY - height;
                        rect.bottom = centerY;
                    }
                    break;
            }
        }
        return true;
    }

    @Override
    public boolean rotate(float degrees) {
        return false;
    }

    @Override
    public boolean scale(float scale) {
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawRect(rect, getPaint());
    }

    @Override
    public boolean hit(float x, float y) {
        return false;
    }

}
