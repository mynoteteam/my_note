package maestro.mynote.ui.painter.shapes;

import android.graphics.Canvas;
import android.util.Pair;

import java.util.ArrayList;

/**
 * Created by Artyom on 29.10.2015.
 */
public class DotsDrawObject extends DrawObject {

    private ArrayList<Pair<Float, Float>> mCoords = new ArrayList<>();

    public DotsDrawObject(DrawCallback callback) {
        super(callback);

        getPaint().setStrokeWidth(4);

    }

    @Override
    public boolean move(float x, float y) {
//        increase();
        mCoords.add(new Pair<Float, Float>(x, y));
        return true;
    }

    @Override
    public boolean rotate(float degrees) {
        return false;
    }

    @Override
    public boolean scale(float scale) {
        return false;
    }

    private void increase() {

    }

    @Override
    public void draw(Canvas canvas) {
        for (Pair<Float, Float> pair : mCoords) {
            canvas.drawPoint(pair.first, pair.second, getPaint());
        }
    }

    @Override
    public boolean hit(float x, float y) {
        return false;
    }

}
