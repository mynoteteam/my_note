package maestro.mynote.ui.painter.shapes;

import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Artyom on 11.11.2015.
 */
public class LineDrawObject extends ShapeDrawObject {

    private float centerX = NOT_SET;
    private float centerY = NOT_SET;

    private float lastX = NOT_SET;
    private float lastY = NOT_SET;

    public LineDrawObject(DrawCallback callback) {
        super(callback);
        getPaint().setStyle(Paint.Style.STROKE);
    }

    @Override
    public boolean move(float x, float y) {
        if (centerX == NOT_SET) {
            centerX = x;
            centerY = y;
        }
        lastX = x;
        lastY = y;
        return true;
    }

    @Override
    public boolean rotate(float degrees) {
        return false;
    }

    @Override
    public boolean scale(float scale) {
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawLine(centerX, centerY, lastX, lastY, getPaint());
    }

    @Override
    public boolean hit(float x, float y) {
        return false;
    }
}
