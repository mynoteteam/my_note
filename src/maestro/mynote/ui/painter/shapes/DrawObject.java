package maestro.mynote.ui.painter.shapes;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

/**
 * Created by Artyom on 29.10.2015.
 */
public abstract class DrawObject {

    public static final int NOT_SET = Integer.MAX_VALUE;

    private DrawCallback mCallback;
    private Paint mPaint;
    private Paint mBufferPaint;
    private Bitmap mBuffer;

    public DrawObject(DrawCallback callback) {
        mCallback = callback;
    }

    public void invalidateSelf() {
        if (mCallback != null) {
            mCallback.invalidate();
        }
    }

    public Paint getPaint() {
        if (mPaint == null) {
            mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        }
        return mPaint;
    }

    public Paint getBufferPaint() {
        if (mBufferPaint == null) {
            mBufferPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        }
        return mBufferPaint;
    }

    public void beforeDraw() {
    }

    public void afterDraw() {
    }

    public void beforeFinalDraw() {
        if (mBuffer != null && !mBuffer.isRecycled()) {
            mBuffer.recycle();
            mBuffer = null;
        }
    }

    public void afterFinalDraw() {
    }

    public void allocateBuffer() {
        if (mBuffer == null) {
            mBuffer = Bitmap.createBitmap(mCallback.getWidth(), mCallback.getHeight(), Bitmap.Config.ARGB_8888);
        }
    }

    public Bitmap getBuffer() {
        return mBuffer;
    }

    public abstract boolean move(float x, float y);

    public abstract boolean rotate(float degrees);

    public abstract boolean scale(float scale);

    public abstract void draw(Canvas canvas);

    public abstract boolean hit(float x, float y);

    public interface DrawCallback {
        void invalidate();

        int getWidth();

        int getHeight();
    }

}
