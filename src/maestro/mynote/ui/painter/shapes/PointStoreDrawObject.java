package maestro.mynote.ui.painter.shapes;

import android.graphics.Matrix;
import android.graphics.Path;
import maestro.mynote.ui.painter.PointF;

/**
 * Created by Artyom on 30.10.2015.
 */
public abstract class PointStoreDrawObject extends DrawObject {

    private Path mPath = new Path();
    private PointF[] mPoints = new PointF[0];
    private int size = 0;

    public PointStoreDrawObject(DrawCallback callback) {
        super(callback);
    }

    @Override
    public boolean hit(float x, float y) {
        if (fastHit(x, y)) {
            final int size = mPoints.length;
            for (int i = 0; i < size; i++) {
                if (mPoints[i].equals(x, y)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean move(float x, float y) {
        increase();
        mPoints[size - 1] = new PointF(x, y);
        return false;
    }

    public Path getPath() {
        return mPath;
    }

    @Override
    public boolean rotate(float degrees) {
        return false;
    }

    @Override
    public boolean scale(float scale) {
        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        mPath.transform(matrix);
        return true;
    }

    private final void increase() {
        PointF[] temp = new PointF[size + 1];
        System.arraycopy(mPoints, 0, temp, 0, size);
        size++;
        mPoints = temp;
    }

    public abstract boolean fastHit(float x, float y);

}
