package maestro.mynote.ui.painter.shapes;

import android.graphics.*;
import android.util.Log;

/**
 * Created by Artyom on 29.10.2015.
 */
public class PathDrawObject extends PointStoreDrawObject {

    private static final CornerPathEffect PRE_DRAW_PATH_EFFECT = new CornerPathEffect(25f);
    private static final CornerPathEffect DRAW_PATH_EFFECT = new CornerPathEffect(3000f);

    private Path mPath;
    private Path mDrawPath;

    private float mPrevX;
    private float mPrevY;

    public PathDrawObject(DrawCallback callback) {
        super(callback);

        getPaint().setStrokeWidth(6);
        getPaint().setDither(true);
        getPaint().setStyle(Paint.Style.STROKE);
        getPaint().setStrokeJoin(Paint.Join.ROUND);
        getPaint().setStrokeCap(Paint.Cap.ROUND);

    }

    @Override
    public boolean move(float x, float y) {
        super.move(x, y);
        if (mPath == null) {
            mPath = new Path();
            mPath.moveTo(x, y);
        } else {
            mPath.lineTo(x, y);
        }
        return true;
    }

    @Override
    public boolean rotate(float degrees) {
        return false;
    }

    @Override
    public boolean scale(float scale) {
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        if (getBuffer() == null) {
            canvas.drawPath(mPath, getPaint());
        } else {
            canvas.drawBitmap(getBuffer(), 0, 0, getBufferPaint());
        }
    }

    private void drawToBuffer() {
        Canvas canvas = new Canvas(getBuffer());
        canvas.drawPath(mPath, getPaint());
    }

    @Override
    public void beforeDraw() {
        super.beforeDraw();
//        getPaint().setPathEffect(PRE_DRAW_PATH_EFFECT);
//        allocateBuffer();
    }

    @Override
    public void beforeFinalDraw() {
        super.beforeFinalDraw();
        getPaint().setPathEffect(DRAW_PATH_EFFECT);
    }

    @Override
    public boolean fastHit(float x, float y) {
        RectF rect = new RectF();
        mPath.computeBounds(rect, true);

        Log.e("TEST", "bounds: " + rect + ", check coords: " + x + "/" + y + ", contains: " + rect.contains(x, y));

        return rect.contains(x, y);
    }

}
