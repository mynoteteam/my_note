package maestro.mynote.ui.painter.shapes;

import maestro.mynote.ui.painter.shapes.DrawObject;

/**
 * Created by Artyom on 03.11.2015.
 */
public abstract class ShapeDrawObject extends DrawObject {

    public ShapeDrawObject(DrawCallback callback) {
        super(callback);
    }
}
