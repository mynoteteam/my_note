package maestro.mynote.ui.painter.shapes;

import android.graphics.Canvas;
import android.graphics.Path;

/**
 * Created by Artyom on 11.11.2015.
 */
public class TriangleDrawObject extends ShapeDrawObject {

    private DRAG_TYPE mDragType = DRAG_TYPE.SIDE;

    public enum DRAG_TYPE {
        SIDE, SCALE, FIXED
    }

    private Path mPath = new Path();
    private float centerX = NOT_SET;
    private float centerY = NOT_SET;

    public TriangleDrawObject(DrawCallback callback) {
        super(callback);
    }

    @Override
    public boolean move(float x, float y) {
        if (centerX == NOT_SET) {
            centerX = x;
            centerY = y;
        }
        float width = 0;
        float height = 0;

        switch (mDragType) {
            case FIXED:
                width = height = (Math.abs(centerX - x) + Math.abs(centerY - y));
                break;
            case SCALE:
                width = Math.abs(centerX - x) * 2f;
                height = Math.abs(centerY - y) * 2f;
                break;
            case SIDE:
                width = Math.abs(centerX - x);
                height = Math.abs(centerY - y);
                break;
        }

        float bx = centerX - width / 2;
        float by = centerY + height / 2;

        mPath.reset();
        mPath.moveTo(bx, by);
        mPath.lineTo(centerX, centerY - height / 2);
        mPath.lineTo(centerX + width / 2, by);
        mPath.lineTo(bx, by);
        return true;
    }

    @Override
    public boolean rotate(float degrees) {
        return false;
    }

    @Override
    public boolean scale(float scale) {
        return false;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawPath(mPath, getPaint());
    }

    @Override
    public boolean hit(float x, float y) {
        return false;
    }
}
