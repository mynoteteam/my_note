package maestro.mynote.ui.painter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import maestro.mynote.ui.painter.shapes.*;

import java.util.ArrayList;

/**
 * Created by Artyom on 29.10.2015.
 */
public class DrawView extends View implements DrawObject.DrawCallback {

    public static final String TAG = DrawView.class.getSimpleName();

    private ArrayList<DrawObject> mDrawObjects = new ArrayList<>();
    private ArrayList<OnDrawViewEventListener> mListeners = new ArrayList<>();
    private DrawObject mCurrentDrawObject;
    private Paint mBufferPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
    private Bitmap mBuffer;
    private GestureDetector mGestureDetector;

    private float mStartX;
    private float mStartY;
    private float mPrevX;
    private float mPrevY;
    private int mActivePointerId;

    private boolean isErase;

    public interface OnDrawViewEventListener {
        void onDrawStart();

        void onDrawEnd();
    }

    public DrawView(Context context) {
        super(context);
        init();
    }

    public DrawView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){}

    public void setErase(boolean erase) {
        isErase = erase;
    }

    public boolean isErase() {
        return isErase;
    }

    public void addOnDrawViewEventListener(OnDrawViewEventListener listener) {
        mListeners.add(listener);
    }

    public void removeOnDrawViewEventListener(OnDrawViewEventListener listener) {
        mListeners.remove(listener);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int action = event.getAction() & event.getActionMasked();

        if (mActivePointerId == NO_ID) {
            mActivePointerId = event.getPointerId(0);
        }

        final float focusX = event.getX(mActivePointerId);
        final float focusY = event.getY(mActivePointerId);

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                notifyStart();
                mStartX = focusX;
                mStartY = focusY;
                if (!isErase) {
                    mCurrentDrawObject = obtainDrawObject();
                    mCurrentDrawObject.beforeDraw();
                }
            case MotionEvent.ACTION_MOVE:
                if (isErase) {
                    ArrayList<DrawObject> eraseObjects = new ArrayList<>();
                    for (DrawObject drawObject : mDrawObjects) {
                        if (drawObject.hit(focusX, focusY)) {
                            eraseObjects.add(drawObject);
                        }
                    }
                    mDrawObjects.remove(eraseObjects);
                    updateBuffer();
                    invalidate();
                } else if (mCurrentDrawObject.move(focusX, focusY)) {
                    invalidate();
                    mCurrentDrawObject.afterDraw();
                }

                mPrevX = focusX;
                mPrevY = focusY;

                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                //TODO: release and save

                mActivePointerId = NO_ID;
                if (mCurrentDrawObject != null) {
                    mDrawObjects.add(mCurrentDrawObject);
                }
                mCurrentDrawObject = null;
                updateBuffer();
                invalidate();
                notifyEnd();
                break;
        }

        return true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mBuffer != null) {
            canvas.drawBitmap(mBuffer, 0, 0, mBufferPaint);
        }
        if (mCurrentDrawObject != null) {
            mCurrentDrawObject.draw(canvas);
        }
    }

    private DrawObject obtainDrawObject() {
        //TODO: add more objects

        if (true) {
            return new TriangleDrawObject(this);
        }

        if (true) {
            return new LineDrawObject(this);
        }

        if (true) {
            return new OvalDrawObject(this);
        }

        if (true) {
            return new RectangleDrawObject(this);
        }

        if (true) {
            return new PathDrawObject(this);
        }

        return new DotsDrawObject(this);
    }

    private void updateBuffer() {
        if (mBuffer == null) {
            mBuffer = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            mBuffer.eraseColor(Color.TRANSPARENT);
        }
        final Canvas canvas = new Canvas(mBuffer);
        for (DrawObject drawObject : mDrawObjects) {
            drawObject.beforeFinalDraw();
            drawObject.draw(canvas);
            drawObject.afterFinalDraw();
        }
    }

    private void notifyStart() {
        for (OnDrawViewEventListener listener : mListeners) {
            listener.onDrawStart();
        }
    }

    private void notifyEnd() {
        for (OnDrawViewEventListener listener : mListeners) {
            listener.onDrawEnd();
        }
    }


}
