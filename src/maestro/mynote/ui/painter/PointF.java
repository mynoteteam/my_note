package maestro.mynote.ui.painter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Artyom on 30.10.2015.
 */
public class PointF implements Parcelable {

    public float x;
    public float y;

    public PointF(float x, float y) {
        this.x = x;
        this.y = y;
    }

    protected PointF(Parcel in) {
        x = in.readFloat();
        y = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(x);
        dest.writeFloat(y);
    }

    public boolean equals(float x, float y) {
        return this.x == x && this.y == y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o instanceof PointF) {
            PointF point = (PointF) o;
            return equals(point.x, point.y);
        }
        return super.equals(o);
    }

    public static final Creator<PointF> CREATOR = new Creator<PointF>() {
        @Override
        public PointF createFromParcel(Parcel in) {
            return new PointF(in);
        }

        @Override
        public PointF[] newArray(int size) {
            return new PointF[size];
        }
    };

}
