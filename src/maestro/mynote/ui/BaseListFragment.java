package maestro.mynote.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import maestro.my.api.MyApi;
import maestro.mynote.MyNoteApplication;
import maestro.mynote.R;
import maestro.mynote.data.model.Note;
import maestro.mynote.utils.TextItemDecoration;
import maestro.mynote.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Artyom on 5/19/2015.
 */
public class BaseListFragment extends BaseFragment implements ChildEventListener {

    public static final String TAG = BaseListFragment.class.getSimpleName();

    private RecyclerView mList;
    private NoteAdapter mAdapter;
    private TextItemDecoration mTextDecoration;
    private SwipeRefreshLayout mRefreshLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.list_fragment_view, null);
        mList = (RecyclerView) v.findViewById(R.id.list);
        mList.setLayoutManager(new GridLayoutManager(getActivity(), getSpanCount()));
        mTextDecoration = new TextItemDecoration(getContext(), R.style.NotesDecorationStyle, getSpanCount());
        mList.addItemDecoration(mTextDecoration);
        mRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new NoteAdapter((MyNoteApplication) getActivity().getApplication()));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (MyApi.getInstance().isAuthorized()) {
            //TODO: implement
            MyApi.getInstance().getUserAppBase().child(NoteData.CHILD_NOTES).addChildEventListener(this);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (MyApi.getInstance().isAuthorized()) {
            MyApi.getInstance().getUserAppBase().child(NoteData.CHILD_NOTES).removeEventListener(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    private void refresh() {
        if (MyApi.getInstance().isAuthorized()) {
            mRefreshLayout.setRefreshing(true);
            MyApi.getInstance().getUserAppBase().child(NoteData.CHILD_NOTES).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    ArrayList<Note> mNotes = new ArrayList<Note>();
                    Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                    while (iterator.hasNext()) {
                        mNotes.add(new Note(iterator.next()));
                    }
                    updateData(mNotes);
                    mRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {
                    mRefreshLayout.setRefreshing(false);
                }
            });
        }
    }

    //TODO: check for performance issues
    private void updateData(ArrayList<Note> notes) {
        Collections.sort(notes, new Comparator<Note>() {
            @Override
            public int compare(Note lhs, Note rhs) {
                String lTitle = lhs.getTitle();
                String rTitle = rhs.getTitle();
                return lTitle == null ? 1 : rTitle == null ? -1 : lTitle.compareTo(rTitle);
            }
        });
        Collections.sort(notes, new Comparator<Note>() {
            @Override
            public int compare(Note lhs, Note rhs) {
                return Utils.compare(lhs.getCreateTime(), rhs.getCreateTime());
            }
        });

        HashMap<Integer, String> dividerPositions = new HashMap<>();

        for (int i = 0; i < notes.size(); i++) {
            long date = notes.get(i).getDateCreateLong();
            String asString = new SimpleDateFormat("dd MMMM yyyy").format(new Date(date));
            if (!dividerPositions.containsValue(asString)) {
                dividerPositions.put(i, asString);
            }
        }
        if (dividerPositions.size() > 0) {
            mTextDecoration.setDividers(dividerPositions);
        } else {
            mTextDecoration.clear();
        }
        mAdapter.update(notes);
        Log.e(TAG, "notes: " + notes.size());
    }

    public int getSpanCount() {
        return 1;
    }

    @Override
    public void onChildAdded(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onChildRemoved(DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

    }

    @Override
    public void onCancelled(FirebaseError firebaseError) {

    }

    public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {

        private MyNoteApplication mApplication;
        private LayoutInflater mInflater;
        private ArrayList<Note> mItems = new ArrayList<>();

        public NoteAdapter(MyNoteApplication application) {
            mApplication = application;
            mInflater = (LayoutInflater) mApplication.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void update(ArrayList<Note> items) {
            if (items != null) {
                mItems = items;
            } else {
                mItems.clear();
            }
            notifyDataSetChanged();
        }

        @Override
        public NoteHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            final NoteHolder holder = new NoteHolder(mInflater.inflate(R.layout.note_item_view, null));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddNoteFragment.makeInstance(mItems.get(holder.getAdapterPosition()))
                            .show(getChildFragmentManager(), AddNoteFragment.TAG);
                }
            });
            return holder;
        }

        @Override
        public void onBindViewHolder(NoteHolder noteHolder, int i) {
            Note note = mItems.get(i);
            noteHolder.Title.setText(note.getTitle());
            noteHolder.Content.setText(note.getContent());
            noteHolder.ColorIcon.setBackgroundColor(note.getColorInt());
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class NoteHolder extends RecyclerView.ViewHolder {

            View ColorIcon;
            TextView Title;
            TextView Content;

            public NoteHolder(View itemView) {
                super(itemView);
                ColorIcon = itemView.findViewById(R.id.color_icon);
                Title = (TextView) itemView.findViewById(R.id.title);
                Content = (TextView) itemView.findViewById(R.id.content);
                ColorIcon.setBackgroundColor(Color.parseColor("#4acccccc"));
            }
        }

    }

}
