package maestro.mynote.ui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.dream.android.mim.RecyclingImageView;
import maestro.my.api.MyApi;
import maestro.my.api.model.User;
import maestro.svg.SVGInstance;
import maestro.mynote.Main;
import maestro.mynote.R;
import maestro.mynote.utils.Typefacer;

import java.util.ArrayList;

/**
 * Created by Artyom on 5/16/2015.
 */
public class NavigationFragment extends BaseFragment implements MyApi.OnUserStateChangeListener {

    public static final String TAG = NavigationFragment.class.getSimpleName();

    private ListView mList;
    private RecyclingImageView mHeaderBackground;
    private RecyclingImageView mHeaderIcon;
    private RecyclingImageView mSecondHeaderIcon;
    private TextView mUserName;
    private TextView mUserEmail;
    private NavigationAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.navigation_view, null);
        mList = (ListView) v.findViewById(R.id.list);
        mList.setDividerHeight(0);
        final View headerView = inflater.inflate(R.layout.navigation_header_view, null);
        mHeaderBackground = (RecyclingImageView) headerView.findViewById(R.id.background_image);
        mHeaderIcon = (RecyclingImageView) headerView.findViewById(R.id.user_icon);
        mSecondHeaderIcon = (RecyclingImageView) headerView.findViewById(R.id.second_user_icon);
        mUserName = (TextView) headerView.findViewById(R.id.title);
        mUserEmail = (TextView) headerView.findViewById(R.id.email);
        mUserName.setTypeface(Typefacer.rMedium);
        mUserEmail.setTypeface(Typefacer.rRegular);
        mList.addHeaderView(headerView);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new NavigationAdapter(getActivity(), collectItems()));
    }

    @Override
    public void onStart() {
        super.onStart();
        MyApi.getInstance().shotAttachOnUserStateChangeListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        MyApi.getInstance().detachOnUserStateChangeListener(this);
    }

    private ArrayList<NavigationItem> collectItems() {
        ArrayList<NavigationItem> items = new ArrayList<NavigationItem>();
        items.add(NavigationItem.space());
        items.add(new NavigationItem(Main.TYPE_BIRTHDAYS, getString(R.string.birthdays), R.raw.cake));
        items.add(new NavigationItem(Main.TYPE_EVENTS, getString(R.string.events), R.raw.alarm));
        items.add(new NavigationItem(Main.TYPE_NOTES, getString(R.string.notes), R.raw.book));
        items.add(NavigationItem.divider());
        items.add(new NavigationItem(Main.TYPE_INVITE, getString(R.string.invite), R.raw.invite));
        items.add(new NavigationItem(Main.TYPE_SETTINGS, getString(R.string.settings), R.raw.settings));
        items.add(NavigationItem.space());
        return items;
    }

    private final void applyUser(User user) {
        if (user != null) {
            mUserName.setText(user.getFirstName() + user.getLastName());
            mUserEmail.setText(user.getEmail());
            mHeaderIcon.setVisibility(View.VISIBLE);
        } else {
            mUserName.setText(R.string.authorize_please);
            mUserEmail.setText(R.string.click_to_authorize);
            mHeaderIcon.setVisibility(View.GONE);
            mSecondHeaderIcon.setVisibility(View.GONE);
        }
    }

    @Override
    public void onUserStateChange(User user, MyApi.UserState state) {
        applyUser(user);
    }

    private final class NavigationAdapter extends BaseAdapter {

        public static final int TYPE_NORMAL = 0;
        public static final int TYPE_SPACE = 1;
        public static final int TYPE_DIVIDER = 2;

        private Context mContext;
        private LayoutInflater mInflater;
        private ArrayList<NavigationItem> mItems;

        public NavigationAdapter(Context context, ArrayList<NavigationItem> items) {
            mContext = context;
            mInflater = LayoutInflater.from(context);
            mItems = items;
        }

        @Override
        public int getCount() {
            return mItems != null ? mItems.size() : 0;
        }

        @Override
        public NavigationItem getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return mItems.get(position).Id;
        }

        @Override
        public int getItemViewType(int position) {
            return mItems.get(position).Type;
        }

        @Override
        public boolean isEnabled(int position) {
            return mItems.get(position).Type == TYPE_NORMAL;
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            int type = getItemViewType(position);
            switch (type) {
                case TYPE_NORMAL:
                    NormalHolder nHolder;
                    if (v == null || !(v.getTag() instanceof NormalHolder)) {
                        v = mInflater.inflate(R.layout.navigation_item_view, null);
                        nHolder = new NormalHolder(v);
                    } else {
                        nHolder = (NormalHolder) v.getTag();
                    }
                    NavigationItem item = mItems.get(position);
                    nHolder.Title.setText(item.Title);
                    if (item.IconResource != -1) {
                        nHolder.Icon.setVisibility(View.VISIBLE);
                        SVGInstance.applySVG(nHolder.Icon, item.IconResource, Color.parseColor("#757575"));
                    }
                    break;
                case TYPE_DIVIDER:
                case TYPE_SPACE:
                    if (v == null || !(v.getTag() instanceof DividerHolder)) {
                        v = mInflater.inflate(R.layout.navigation_divider_item, null);
                        new DividerHolder(v, type);
                    } else {
                        ((DividerHolder) v.getTag()).apply(type);
                    }
                    break;
            }
            return v;
        }

        class NormalHolder {

            ImageView Icon;
            TextView Title;

            NormalHolder(View v) {
                Icon = (ImageView) v.findViewById(R.id.image);
                Title = (TextView) v.findViewById(R.id.title);
                Title.setTypeface(Typefacer.rMedium);
                v.setTag(this);
            }

        }

        class DividerHolder {

            View Divider;

            public DividerHolder(View v, int type) {
                Divider = v.findViewById(R.id.divider);
                apply(type);
                v.setTag(this);
            }

            public void apply(int type) {
                Divider.setVisibility(type == TYPE_DIVIDER ? View.VISIBLE : View.GONE);
            }

        }

    }

    public static final class NavigationItem {

        private int Id;
        private int Type = NavigationAdapter.TYPE_NORMAL;
        private String Title;
        private int IconResource = -1;

        public static NavigationItem divider() {
            return new NavigationItem(NavigationAdapter.TYPE_DIVIDER);
        }

        public static NavigationItem space() {
            return new NavigationItem(NavigationAdapter.TYPE_SPACE);
        }

        public NavigationItem(int type) {
            Type = type;
        }

        public NavigationItem(int id, String title) {
            Id = id;
            Title = title;
        }

        public NavigationItem(int id, String title, int iconResourse) {
            this(id, title);
            IconResource = iconResourse;
        }

    }

}
