package maestro.mynote;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

/**
 * Created by Artyom on 5/19/2015.
 */
public class NotifyService extends Service {

    private static final int NOTIFICATION_ID = 0;

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!");

        NotificationManagerCompat.from(getApplicationContext()).notify(NOTIFICATION_ID, mBuilder.build());

    }

    public class LocalBinder extends Binder {
        NotifyService notifyService;

        public LocalBinder(NotifyService service) {
            notifyService = service;
        }

        public NotifyService getNotifyService() {
            return notifyService;
        }
    }

}
