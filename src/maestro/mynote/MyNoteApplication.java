package maestro.mynote;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import maestro.my.api.MyApi;
import maestro.mynote.utils.Typefacer;
import maestro.svg.SVGInstance;

/**
 * Created by Artyom on 5/16/2015.
 */
public class MyNoteApplication extends Application {

    private NotifyService mService;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mService = ((NotifyService.LocalBinder) service).getNotifyService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Typefacer.initialize(this);
        SVGInstance.initialize(this);
        MyApi.getInstance().initialize(this, "my-note.firebaseIO.com", "my_note");
        bindService(new Intent(this, NotifyService.class), mConnection, BIND_AUTO_CREATE);

//        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
//            @Override
//            public void uncaughtException(Thread thread, final Throwable ex) {
//                new Thread() {
//                    @Override
//                    public void run() {
//                        super.run();
//                        try {
//                            String url = "https://rink.hockeyapp.net/api/2/apps/" + "f38a04b2237b4caea62df5da909b983f" + "/crashes";
//                            List<NameValuePair> parameters = new ArrayList<NameValuePair>();
//                            PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//                            StringBuilder log = new StringBuilder();
//                            log.append("Package: " + getPackageName() + "\n");
//                            log.append("Version: " + pinfo.versionCode + "\n");
//                            log.append("Android: " + Build.VERSION.SDK_INT + "\n");
//                            log.append("Manufacturer: " + android.os.Build.MANUFACTURER + "\n");
//                            log.append("Model: " + Build.DEVICE + "\n");
//                            log.append("Date: " + new Date() + "\n");
//
//                            for (StackTraceElement element : ex.getStackTrace()) {
//                                log.append("\n");
//                                log.append(element.toString());
//                            }
//
//                            DefaultHttpClient httpClient = new DefaultHttpClient();
//                            HttpPost httpPost = new HttpPost(url);
//
//                            parameters.add(new BasicNameValuePair("raw", log.toString()));
//                            httpPost.setEntity(new UrlEncodedFormEntity(parameters, HTTP.UTF_8));
//
//                            HttpResponse response = httpClient.execute(httpPost);
//                            Log.e("TEST", "response code: " + response.getStatusLine().getStatusCode());
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        System.exit(2);
//                    }
//                }.start();
//            }
//        });

    }

}
