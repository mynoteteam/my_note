package maestro.mynote.utils;

import android.graphics.Color;

/**
 * Created by Artyom on 5/17/2015.
 */
public class Utils {

    public static final String MIM_NETWORK = "mim_network";

    public static boolean isBrightColor(String color) {
        return isBrightColor(Color.parseColor(color));
    }

    public static boolean isBrightColor(int color) {
        if (android.R.color.transparent == color)
            return true;
        final int[] rgb = {Color.red(color), Color.green(color), Color.blue(color)};
        final int brightness = (int) Math.sqrt(rgb[0] * rgb[0] * .241 + rgb[1] * rgb[1] * .691 + rgb[2] * rgb[2] * .068);
        if (brightness >= 200) {
            return true;
        }
        return false;
    }

    public static int compare(long lhs, long rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }

    public static boolean isSame(Object obj1, Object obj2) {
        return obj1 != null && obj2 != null && obj1.equals(obj2);
    }

}
