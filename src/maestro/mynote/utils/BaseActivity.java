package maestro.mynote.utils;

import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

/**
 * Created by Artyom on 10.11.2015.
 */
public class BaseActivity extends AppCompatActivity {

    private ArrayList<OnBackPressListener> mListeners = new ArrayList<>();

    public void attachOnBackPressListener(OnBackPressListener listener) {
        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    public void detachOnBackPressListener(OnBackPressListener listener) {
        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    @Override
    public void onBackPressed() {
        synchronized (mListeners) {
            for (OnBackPressListener listener : mListeners) {
                if (listener.onBackPress()) {
                    return;
                }
            }
        }
        super.onBackPressed();
    }
}
