package maestro.mynote.utils;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Log;

/**
 * Created by Artyom on 7/1/2015.
 */
public class ColorCircle extends Drawable {

    public static final String TAG = ColorCircle.class.getSimpleName();

    private Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mStrokePaint;

    public ColorCircle(int color) {
        mPaint.setColor(color);
    }

    public ColorCircle(int color, int strokeColor, int strokeWidth) {
        mPaint.setColor(color);
        mStrokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mStrokePaint.setStyle(Paint.Style.STROKE);
        mStrokePaint.setColor(strokeColor);
        mStrokePaint.setStrokeWidth(strokeWidth);
    }

    @Override
    public void draw(Canvas canvas) {
        Rect rect = getBounds();
        canvas.drawCircle(rect.centerX(), rect.centerY(), rect.width() / 2, mPaint);
        if (mStrokePaint != null) {
            canvas.drawCircle(rect.centerX(), rect.centerY(),
                    rect.width() / 2 - mStrokePaint.getStrokeWidth() / 2, mStrokePaint);
        }
    }

    public void setColor(int color) {
        mPaint.setColor(color);
        invalidateSelf();
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return mPaint.getAlpha();
    }
}
