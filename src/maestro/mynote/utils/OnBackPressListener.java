package maestro.mynote.utils;

/**
 * Created by Artyom on 10.11.2015.
 */
public interface OnBackPressListener {

    boolean onBackPress();

}
