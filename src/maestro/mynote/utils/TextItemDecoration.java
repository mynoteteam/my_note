package maestro.mynote.utils;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import maestro.mynote.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Artyom on 7/6/2015.
 */
public class TextItemDecoration extends RecyclerView.ItemDecoration {

    public static final String TAG = TextItemDecoration.class.getSimpleName();

    private Map<Integer, String> mDividers;
    private Paint mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private int mSpanCount;
    private int mTextSize;
    private int mHeight;
    private int mTextColor;
    private int paddingLeft;
    private int paddingRight;
    private int textTopPadding;

    public TextItemDecoration(Context context, int styleResource, int spanCount) {
        final TypedArray attrs = context.obtainStyledAttributes(styleResource, R.styleable.TextDecoration);
        final Resources resources = context.getResources();
        final DisplayMetrics metrics = resources.getDisplayMetrics();
        final int defPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, metrics);

        mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        mTextSize = attrs.getDimensionPixelSize(R.styleable.TextDecoration_textColor,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, metrics));
        mHeight = attrs.getDimensionPixelSize(R.styleable.TextDecoration_textContainerHeight,
                (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 48, metrics));
        mTextColor = attrs.getColor(R.styleable.TextDecoration_textColor, Color.BLACK);
        paddingLeft = attrs.getDimensionPixelSize(R.styleable.TextDecoration_textPaddingLeft, defPadding);
        paddingRight = attrs.getDimensionPixelSize(R.styleable.TextDecoration_textPaddingRight, defPadding);
        mSpanCount = spanCount;

        Rect bounds = new Rect();
        mTextPaint.getTextBounds("A", 0, 1, bounds);
        textTopPadding = bounds.height();

        mTextPaint.setColor(mTextColor);
        mTextPaint.setTextSize(mTextSize);

    }

    public void setDividers(Map<Integer, String> dividers) {
        mDividers = dividers;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        final int position = parent.getChildAdapterPosition(view);
        if (mDividers != null && mDividers.size() > 0) {
            if (mDividers.containsKey(position)) {
                outRect.top += mHeight;
            }
        }

    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);
        if (mDividers != null && mDividers.size() > 0) {
            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View v = parent.getChildAt(i);
                int position = parent.getChildAdapterPosition(v);

                if (mDividers.containsKey(position)) {
                    //TODO: add ellipsize
//                    float width = mTextPaint.measureText(mDividers.get(position));
//                    String drawText;
//                    if (width > parent.getWidth()){
//                        dr
//                    }
                    c.drawText(mDividers.get(position), v.getX() + paddingLeft, v.getY() + textTopPadding - mHeight / 2, mTextPaint);
                }
            }
        }
    }

    public void clear() {
        mDividers.clear();
        mDividers = null;
    }

    public interface ItemDataRetriever<T> {
        void retrieveTitle(T item);

        void retrieveText(T item);
    }

    public static <T> HashMap<Integer, String> indexize(ArrayList<T> items, ItemDataRetriever<T> retriever) {
        return null;
    }

}
