package maestro.mynote;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Artyom on 29.10.2015.
 */
public class DrawActivity extends AppCompatActivity {

    public static final String TAG = DrawActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.draw_activity_view);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame, new DrawFragment(), DrawFragment.TAG)
                    .commit();
        }
    }

}