package maestro.mynote;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import maestro.my.api.LoginActivity;
import maestro.my.api.MyApi;
import maestro.my.api.model.User;
import maestro.mynote.ui.AddNoteFragment;
import maestro.mynote.ui.BaseListFragment;
import maestro.mynote.ui.NavigationFragment;
import maestro.mynote.utils.BaseActivity;
import maestro.svg.SVG;
import maestro.svg.SVGInstance;

public class Main extends BaseActivity implements MyApi.OnUserStateChangeListener {

    public static final String TAG = Main.class.getSimpleName();

    public static final String PARAM_OPEN_EDIT = "open_edit";

    public static final int REQ_CODE_GOOGLE_AUTHORIZE = 1001;

    public static final int TYPE_BIRTHDAYS = 0;
    public static final int TYPE_EVENTS = 1;
    public static final int TYPE_NOTES = 2;
    public static final int TYPE_INVITE = 3;
    public static final int TYPE_SETTINGS = 4;

    private Toolbar mToolbar;
    private DrawerLayout mDrawer;
    private ImageView mAddNoteBtn;
    private ActionBarDrawerToggle mDrawerToggle;
    private FragmentTransaction mTransaction;
    private View mContentParent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mContentParent = findViewById(R.id.content_parent);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, R.string.app_name, R.string.app_name) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                commitTransaction();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mContentParent.setTranslationX(mContentParent.getWidth() * slideOffset * .3f);
            }
        };
        mDrawer.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mContentParent.setTranslationX(mContentParent.getWidth() * slideOffset * .1f);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                commitTransaction();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mAddNoteBtn = (ImageView) findViewById(R.id.add_note_button);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.navigation_frame, new NavigationFragment(), NavigationFragment.TAG)
                    .commit();
        }

        SVG background = SVGInstance.getDrawable(R.raw.el_primary_action_button, Color.parseColor("#444444"), getResources().getColor(R.color.colorPrimary));
        mAddNoteBtn.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mAddNoteBtn.setBackgroundDrawable(background);

        SVG addIcon = SVGInstance.getDrawable(R.raw.add, Color.WHITE);
        mAddNoteBtn.setImageDrawable(addIcon);
        mAddNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddNoteView();
            }
        });

        if (savedInstanceState == null) {
            if (getIntent() != null && getIntent().hasExtra(PARAM_OPEN_EDIT)) {
                showAddNoteView();
            }
            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new BaseListFragment(), BaseListFragment.TAG).commit();
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.hasExtra(PARAM_OPEN_EDIT)) {
            showAddNoteView();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (!MyApi.getInstance().isAuthorized()) {
            startActivityForResult(new Intent(this, LoginActivity.class), 0);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyApi.getInstance().shotAttachOnUserStateChangeListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        MyApi.getInstance().detachOnUserStateChangeListener(this);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            toggleDrawer();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.START)) {
            mDrawer.closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    public void showAddNoteView() {
        new AddNoteFragment().show(getSupportFragmentManager(), AddNoteFragment.TAG);
    }

    public void toggleDrawer() {
        if (!mDrawer.isDrawerOpen(Gravity.START)) {
            mDrawer.openDrawer(Gravity.START);
        } else mDrawer.closeDrawers();
    }

    public void commitTransaction() {
        if (mTransaction != null && !mDrawer.isDrawerOpen(Gravity.START)) {
            mTransaction.commit();
        }
    }

    @Override
    public void onUserStateChange(User user, MyApi.UserState state) {
        switch (state) {
            case LoggedIn:

                break;
            case Idle:

                break;
        }
    }
}
