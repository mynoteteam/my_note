package maestro.mynote.data.model;

import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import maestro.my.api.MyApi;
import maestro.my.api.MyApiUtils;
import maestro.mynote.ui.painter.shapes.DrawObject;
import maestro.mynote.utils.Utils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * {
 * "id":10030
 * "parentId":21
 * "title":"Some title",
 * "content":"Some content",
 * "createTime":1029301930,
 * "notifyTime":1231230980,
 * "type":"SIMPLE",
 * "isBirthday":false,
 * "color": "#4acccccc"
 * "drawables":[]
 * }
 */

public class Note implements Parcelable, Cloneable {

    public static final String TAG = Note.class.getSimpleName();

    public enum TYPE {
        SIMPLE, CHECK_LIST
    }

    private String id;
    private long parentId = NOT_SET;
    private String key;
    private String title;
    private String content;
    private long createTime;
    private long notifyTime;
    private TYPE type = TYPE.SIMPLE;
    private boolean isBirthday;
    private String color = "#FFD600";
    private ArrayList<DrawObject> drawObjects = new ArrayList<>();

    public Note() {
        this.createTime = Calendar.getInstance().getTime().getTime();
    }

    public Note(String title, String content) {
        this();
        this.title = content;
        this.content = content;
    }

    public static final String CHILD_ID = "id";
    public static final String CHILD_PARENT_ID = "parentId";
    public static final String CHILD_TITLE = "title";
    public static final String CHILD_CONTENT = "content";
    public static final String CHILD_CREATE_TIME = "createTime";
    public static final String CHILD_NOTIFY_TIME = "notifyTime";
    public static final String CHILD_TYPE = "type";
    public static final String CHILD_IS_BIRTHDAY = "isBirthday";
    public static final String CHILD_COLOR = "color";

    public static final int NOT_SET = -1;

    public Note(DataSnapshot snapshot) {
        id = MyApiUtils.obtainString(snapshot, CHILD_ID);
        parentId = MyApiUtils.obtainLong(snapshot, CHILD_PARENT_ID);
        title = MyApiUtils.obtainString(snapshot, CHILD_TITLE);
        content = MyApiUtils.obtainString(snapshot, CHILD_CONTENT);
        createTime = MyApiUtils.obtainLong(snapshot, CHILD_CREATE_TIME);
        notifyTime = MyApiUtils.obtainLong(snapshot, CHILD_NOTIFY_TIME);
        type = TYPE.valueOf(MyApiUtils.obtainString(snapshot, CHILD_TYPE, type.name()));
        isBirthday = MyApiUtils.obtainBoolean(snapshot, CHILD_IS_BIRTHDAY, false);
        color = MyApiUtils.obtainString(snapshot, CHILD_COLOR, color);
    }

    public Note(JSONObject jsonObject) {
        id = jsonObject.optString(CHILD_ID);
        parentId = jsonObject.optInt(CHILD_PARENT_ID);
        title = jsonObject.optString(CHILD_TITLE);
        content = jsonObject.optString(CHILD_CONTENT);
        createTime = jsonObject.has(CHILD_CREATE_TIME) && !TextUtils.isEmpty(jsonObject.optString(CHILD_CREATE_TIME))
                ? jsonObject.optLong(CHILD_CREATE_TIME) : Calendar.getInstance().getTime().getTime();
        createTime = jsonObject.has(CHILD_NOTIFY_TIME) ? jsonObject.optLong(CHILD_NOTIFY_TIME) : -1;
        type = TYPE.valueOf(jsonObject.optString(CHILD_TYPE));
        isBirthday = jsonObject.optBoolean(CHILD_IS_BIRTHDAY);
        color = jsonObject.optString(CHILD_COLOR);
    }

    public Map<String, Object> asMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(CHILD_ID, id);
        map.put(CHILD_PARENT_ID, parentId);
        map.put(CHILD_TITLE, title);
        map.put(CHILD_CONTENT, content);
        map.put(CHILD_CREATE_TIME, createTime);
        map.put(CHILD_NOTIFY_TIME, notifyTime);
        map.put(CHILD_TYPE, type);
        map.put(CHILD_IS_BIRTHDAY, isBirthday);
        map.put(CHILD_COLOR, color);
        return map;
    }

    public synchronized void save() {
        final Firebase base = MyApi.getInstance().getUserAppBase().child(NoteData.CHILD_NOTES);
        if (id == null) {
            Firebase nRef = base.push();
            nRef.setValue(asMap());
            id = nRef.getKey();
        } else {
            base.child(String.valueOf(id)).setValue(asMap());
        }
    }

    private Note(Parcel in) {
        id = in.readString();
        key = in.readString();
        title = in.readString();
        content = in.readString();
        createTime = in.readLong();
        notifyTime = in.readLong();
        type = (TYPE) in.readSerializable();
        isBirthday = in.readByte() == 0x00;
        color = in.readString();
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setTitle(String subject) {
        this.title = subject;
    }

    public String getTitle() {
        return this.title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return this.content;
    }

    public void setCreateTime(long date) {
        this.createTime = date;
    }

    public long getCreateTime() {
        return createTime;
    }

    public long getDateCreateLong() {
        return createTime;
    }

    public void setNotifyTime(long notifyTime) {
        this.notifyTime = notifyTime;
    }

    public long getNotifyTime() {
        return notifyTime;
    }

    public long getDateNotifyLong() {
        return notifyTime;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public TYPE getType() {
        return type;
    }

    public void setIsBirthday(boolean isBirthday) {
        this.isBirthday = isBirthday;
    }

    public boolean isBirthday() {
        return isBirthday;
    }

    public boolean isEvent() {
        return notifyTime != NOT_SET;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(int color) {
        this.color = "#" + Integer.toHexString(color).substring(2);
    }

    public int getColorInt() {
        return Color.parseColor(color);
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(title) && TextUtils.isEmpty(content);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(key);
        dest.writeString(title);
        dest.writeString(content);
        dest.writeLong(createTime);
        dest.writeLong(notifyTime);
        dest.writeSerializable(type);
        dest.writeByte((byte) (isBirthday ? 0x00 : 0x01));
        dest.writeString(color);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Note) {
            Note note = (Note) o;
            return note.getId() == getId()
                    && Utils.isSame(key, note.key)
                    && Utils.isSame(title, note.title)
                    && Utils.isSame(content, note.content)
                    && createTime == note.createTime
                    && notifyTime == note.notifyTime
                    && type == note.type
                    && isBirthday == note.isBirthday
                    && Utils.isSame(color, note.color);
        }
        return super.equals(o);
    }

    @Override
    public Note clone() {
        try {
            return (Note) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

}
